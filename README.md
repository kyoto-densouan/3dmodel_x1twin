# README #

1/3スケールのSHARP X-1twin風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- シャープ

## 発売時期
- 1987年12月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/X1_(%E3%82%B3%E3%83%B3%E3%83%94%E3%83%A5%E3%83%BC%E3%82%BF))
- [懐かしのパソコン](https://greendeepforest.com/?p=2401)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_x1twin/raw/31131fe6511ea9fce81190cf0875f395a6d17cbf/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_x1twin/raw/31131fe6511ea9fce81190cf0875f395a6d17cbf/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_x1twin/raw/31131fe6511ea9fce81190cf0875f395a6d17cbf/ExampleImage.jpg)
